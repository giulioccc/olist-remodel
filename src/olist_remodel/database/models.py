from sqlalchemy import (
    Column,
    Date,
    DateTime,
    ForeignKey,
    Integer,
    Numeric,
    String,
    Text,
)
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship

DeclarativeBaseSource = declarative_base()
DeclarativeBaseTarget = declarative_base()


class Geolocation(DeclarativeBaseSource):
    __tablename__ = "geolocations"
    id = Column(Integer, primary_key=True)
    geolocation_zip_code_prefix = Column(String(5))
    geolocation_lat = Column(String(30))
    geolocation_lng = Column(String(30))
    geolocation_city = Column(Text)
    geolocation_state = Column(String(2))


class Customer(DeclarativeBaseSource):
    __tablename__ = "customers"
    customer_id = Column(String(32), primary_key=True)
    customer_unique_id = Column(String(32))
    customer_zip_code_prefix = Column(String(5))
    customer_city = Column(Text)
    customer_state = Column(String(2))
    orders = relationship("Order", back_populates="customer")


class Seller(DeclarativeBaseSource):
    __tablename__ = "sellers"
    seller_id = Column(String(32), primary_key=True)
    seller_zip_code_prefix = Column(String(5))
    seller_city = Column(Text)
    seller_state = Column(String(2))
    items = relationship("Item", back_populates="seller")


class Order(DeclarativeBaseSource):
    __tablename__ = "orders"
    order_id = Column(String(32), primary_key=True)
    order_status = Column(Text)
    order_purchase_timestamp = Column(DateTime)
    order_approved_at = Column(DateTime)
    order_delivered_carrier_date = Column(DateTime)
    order_delivered_customer_date = Column(DateTime)
    order_estimated_delivery_date = Column(DateTime)
    customer_id = Column(String(32), ForeignKey("customers.customer_id"))
    customer = relationship("Customer", back_populates="orders")
    reviews = relationship("Review", back_populates="order")
    payments = relationship("Payment", back_populates="order")
    items = relationship("Item", back_populates="order")


class Product(DeclarativeBaseSource):
    __tablename__ = "products"
    product_id = Column(String(32), primary_key=True)
    product_category_name = Column(Text)
    product_name_lenght = Column(Integer)
    product_description_lenght = Column(Integer)
    product_photos_qty = Column(Integer)
    product_weight_g = Column(Integer)
    product_length_cm = Column(Integer)
    product_height_cm = Column(Integer)
    product_width_cm = Column(Integer)
    items = relationship("Item", back_populates="product")


class Item(DeclarativeBaseSource):
    __tablename__ = "items"
    id = Column(Integer, primary_key=True)
    order_item_id = Column(Integer)
    shipping_limit_date = Column(DateTime)
    price = Column(Numeric)
    freight_value = Column(Numeric)
    order_id = Column(String(32), ForeignKey("orders.order_id"))
    order = relationship("Order", back_populates="items")
    product_id = Column(String(32), ForeignKey("products.product_id"))
    product = relationship("Product", back_populates="items")
    seller_id = Column(String(32), ForeignKey("sellers.seller_id"))
    seller = relationship("Seller", back_populates="items")


class Review(DeclarativeBaseSource):
    __tablename__ = "reviews"
    id = Column(Integer, primary_key=True)
    review_id = Column(String(32))
    review_score = Column(Integer)
    review_comment_title = Column(Text)
    review_comment_message = Column(Text)
    review_creation_date = Column(DateTime)
    review_answer_timestamp = Column(DateTime)
    order_id = Column(String(32), ForeignKey("orders.order_id"))
    order = relationship("Order", back_populates="reviews")


class Payment(DeclarativeBaseSource):
    __tablename__ = "payments"
    id = Column(Integer, primary_key=True)
    payment_sequential = Column(Integer)
    payment_type = Column(Text)
    payment_installments = Column(Integer)
    payment_value = Column(Numeric)
    order_id = Column(String(32), ForeignKey("orders.order_id"))
    order = relationship("Order", back_populates="payments")


class FactReview(DeclarativeBaseTarget):
    __tablename__ = "fact_reviews"
    review_qty = Column(Integer)
    creation_time_timestamp = Column(
        DateTime, ForeignKey("dim_times.time_timestamp"), primary_key=True
    )
    answer_time_timestamp = Column(
        DateTime, ForeignKey("dim_times.time_timestamp"), primary_key=True
    )
    geolocation_id = Column(
        Integer, ForeignKey("dim_geolocations.id"), primary_key=True
    )
    payment_id = Column(Integer, ForeignKey("dim_payments.id"), primary_key=True)
    product_id = Column(Integer, ForeignKey("dim_products.id"), primary_key=True)
    customer_id = Column(Integer, ForeignKey("dim_customers.id"), primary_key=True)


class FactOrder(DeclarativeBaseTarget):
    __tablename__ = "fact_orders"
    order_qty = Column(Integer)
    purchase_time_timestamp = Column(
        DateTime, ForeignKey("dim_times.time_timestamp"), primary_key=True
    )
    approved_at_time_timestamp = Column(
        DateTime, ForeignKey("dim_times.time_timestamp"), primary_key=True
    )
    delivered_carrier_time_timestamp = Column(
        DateTime, ForeignKey("dim_times.time_timestamp"), primary_key=True
    )
    delivered_customer_time_timestamp = Column(
        DateTime, ForeignKey("dim_times.time_timestamp"), primary_key=True
    )
    estimated_delivery_time_timestamp = Column(
        DateTime, ForeignKey("dim_times.time_timestamp"), primary_key=True
    )
    geolocation_id = Column(
        Integer, ForeignKey("dim_geolocations.id"), primary_key=True
    )
    payment_id = Column(Integer, ForeignKey("dim_payments.id"), primary_key=True)
    product_id = Column(Integer, ForeignKey("dim_products.id"), primary_key=True)
    customer_id = Column(Integer, ForeignKey("dim_customers.id"), primary_key=True)


class DimTime(DeclarativeBaseTarget):
    __tablename__ = "dim_times"
    time_timestamp = Column(DateTime, primary_key=True)
    time_date = Column(Date)
    time_year = Column(Integer)
    time_month = Column(Integer)
    time_day = Column(Integer)
    time_hour = Column(Integer)
    time_minute = Column(Integer)
    time_second = Column(Integer)
    purchase_orders = relationship(
        "FactOrder", foreign_keys=[FactOrder.purchase_time_timestamp]
    )
    approved_at_orders = relationship(
        "FactOrder", foreign_keys=[FactOrder.approved_at_time_timestamp]
    )
    delivered_carrier_orders = relationship(
        "FactOrder", foreign_keys=[FactOrder.delivered_carrier_time_timestamp]
    )
    delivered_customer_orders = relationship(
        "FactOrder", foreign_keys=[FactOrder.delivered_customer_time_timestamp]
    )
    estimated_delivery_orders = relationship(
        "FactOrder", foreign_keys=[FactOrder.estimated_delivery_time_timestamp]
    )
    creation_reviews = relationship(
        "FactReview", foreign_keys=[FactReview.creation_time_timestamp]
    )
    answer_reviews = relationship(
        "FactReview", foreign_keys=[FactReview.answer_time_timestamp]
    )


class DimGeolocation(DeclarativeBaseTarget):
    __tablename__ = "dim_geolocations"
    id = Column(Integer, primary_key=True)
    geolocation_id = Column(Integer)
    geolocation_zip_code_prefix = Column(String(5))
    geolocation_lat = Column(String(30))
    geolocation_lng = Column(String(30))
    geolocation_city = Column(Text)
    geolocation_state = Column(String(2))
    orders = relationship("FactOrder", foreign_keys=[FactOrder.geolocation_id])
    reviews = relationship("FactReview", foreign_keys=[FactReview.geolocation_id])


class DimPayment(DeclarativeBaseTarget):
    __tablename__ = "dim_payments"
    id = Column(Integer, primary_key=True)
    payment_id = Column(Integer)
    payment_sequential = Column(Integer)
    payment_type = Column(Text)
    payment_installments = Column(Integer)
    payment_value = Column(Numeric)
    orders = relationship("FactOrder", foreign_keys=[FactOrder.payment_id])
    reviews = relationship("FactReview", foreign_keys=[FactReview.payment_id])


class DimProduct(DeclarativeBaseTarget):
    __tablename__ = "dim_products"
    id = Column(Integer, primary_key=True)
    product_id = Column(String(32))
    product_category_name = Column(Text)
    product_name_lenght = Column(Integer)
    product_description_lenght = Column(Integer)
    product_photos_qty = Column(Integer)
    product_weight_g = Column(Integer)
    product_length_cm = Column(Integer)
    product_height_cm = Column(Integer)
    product_width_cm = Column(Integer)
    orders = relationship("FactOrder", foreign_keys=[FactOrder.product_id])
    reviews = relationship("FactReview", foreign_keys=[FactReview.product_id])


class DimCustomer(DeclarativeBaseTarget):
    __tablename__ = "dim_customers"
    id = Column(Integer, primary_key=True)
    customer_id = Column(String(32))
    customer_unique_id = Column(String(32))
    customer_zip_code_prefix = Column(String(5))
    customer_city = Column(Text)
    customer_state = Column(String(2))
    orders = relationship("FactOrder", foreign_keys=[FactOrder.customer_id])
    reviews = relationship("FactReview", foreign_keys=[FactReview.customer_id])
