CREATE TRIGGER order_notify AFTER INSERT ON orders
FOR EACH ROW EXECUTE PROCEDURE notify_trigger(
  'order_id',
  'order_status',
  'order_purchase_timestamp',
  'order_approved_at',
  'order_delivered_carrier_date',
  'order_delivered_customer_date',
  'order_estimated_delivery_date',
  'customer_id'
);
CREATE TRIGGER review_notify AFTER INSERT ON reviews
FOR EACH ROW EXECUTE PROCEDURE notify_trigger(
  'id',
  'review_id',
  'review_score',
  'review_comment_title',
  'review_comment_message',
  'review_creation_date',
  'review_answer_timestamp',
  'order_id'
);
