"""Pipelines declaration."""

from typing import Dict

from kedro.pipeline import Pipeline, pipeline

from olist_remodel.pipelines import (
    report,
    source_seed_tables,
    source_to_target_etl,
    target_seed_tables,
)


def create_source_seed_pipelines() -> Dict[str, Pipeline]:
    """Creates pipelines for seeding source tables."""
    base_pipeline = source_seed_tables.create_pipeline()
    tables = [
        "geolocations",
        "customers",
        "sellers",
        "orders",
        "products",
        "items",
        "reviews",
        "payments",
    ]
    pipelines = {}
    for table in tables:
        pipelines[f"source_seed_{table}"] = pipeline(
            base_pipeline,
            inputs={"source_table_name_csv": f"source_{table}_csv"},
            outputs={"source_seed_table_name_table": f"source_seed_{table}_table"},
        )
    return pipelines


def create_target_seed_pipelines() -> Dict[str, Pipeline]:
    """Creates pipelines for seeding target tables."""
    return {
        "target_seed_tables": target_seed_tables.create_pipeline(),
    }


def create_source_to_target_etl_pipelines() -> Dict[str, Pipeline]:
    """Creates pipelines for executing ETL from source to target databases."""
    return {
        "source_to_target_etl": source_to_target_etl.create_pipeline(),
    }


def create_report_pipelines() -> Dict[str, Pipeline]:
    """Creates pipelines reporting metrics."""
    return {
        "report": report.create_pipeline(),
    }


def create_pipelines(**kwargs) -> Dict[str, Pipeline]:
    """Creates the project's pipeline.
    """
    return {
        **create_source_seed_pipelines(),
        **create_target_seed_pipelines(),
        **create_source_to_target_etl_pipelines(),
        **create_report_pipelines(),
    }
