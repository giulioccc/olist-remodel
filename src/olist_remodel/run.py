"""Application entry point."""

from pathlib import Path
from typing import Dict

from kedro.framework.context import KedroContext, load_package_context
from kedro.pipeline import Pipeline

from olist_remodel.pipeline import create_pipelines


class ProjectContext(KedroContext):
    project_name = "Olist Remodeling Architecture"
    # `project_version` is the version of kedro used to generate the project
    project_version = "0.16.4"
    package_name = "olist_remodel"

    def _get_pipelines(self) -> Dict[str, Pipeline]:
        return create_pipelines()


def run_package():
    project_context = load_package_context(
        project_path=Path.cwd(), package_name=Path(__file__).resolve().parent.name
    )
    project_context.run()


if __name__ == "__main__":
    run_package()
