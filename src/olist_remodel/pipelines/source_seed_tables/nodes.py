"""Nodes for seeding source database tables with csv data.
"""

import logging

import pandas as pd

from .utils import connect_to_database, create_table, source_credentials

logger = logging.getLogger(__name__)


def seed_table(df: pd.DataFrame, table_name: str) -> pd.DataFrame:
    """Seed table with content of a Pandas DataFrame."""
    connection_uri = source_credentials()["con"]
    connection = connect_to_database(connection_uri)
    create_table(connection, table_name)
    return df
