"""Pipelines for seeding source database tables with csv data.
"""

from kedro.pipeline import Pipeline, node

from .nodes import seed_table


def create_pipeline(**kwargs) -> Pipeline:
    return Pipeline(
        [
            node(
                func=seed_table,
                inputs=["source_table_name_csv", "params:source_table_name"],
                outputs="source_seed_table_name_table",
                name="source_seed_table",
            ),
        ]
    )
