import logging
from typing import Dict

import pandas as pd
from kedro.config import ConfigLoader
from sqlalchemy import create_engine
from sqlalchemy.engine.base import Engine

from olist_remodel.database.models import DeclarativeBaseTarget

logger = logging.getLogger(__name__)


def target_credentials() -> Dict[str, str]:
    """Fetches target database credentials."""
    conf_paths = ["conf/local"]
    conf_loader = ConfigLoader(conf_paths)
    credentials = conf_loader.get("credentials*", "credentials*/**")
    return credentials["target_credentials"]


def connect_to_database(uri: str) -> Engine:
    """Connects to SQL Database."""
    engine = create_engine(uri)
    logger.info(f"Connection established with {uri}")
    return engine


def create_tables(connection: Engine):
    """Creates SQL Tables."""
    DeclarativeBaseTarget.metadata.create_all(connection)
    logger.info("Tables created")


def data_catalog() -> Dict[str, dict]:
    """Fetches this pipeline's Data Catalog."""
    conf_paths = ["conf/base", "conf/local"]
    conf_loader = ConfigLoader(conf_paths)
    catalog = conf_loader.get(
        "catalog*",
        "catalog*/**",
        "pipelines/target_seed_tables/catalog*",
        "pipelines/target_seed_tables/catalog*/**",
    )
    return catalog


def seed_dim_times_table(
    source_orders: pd.DataFrame, source_reviews: pd.DataFrame, connection: Engine,
):
    df_time_columns = [
        (
            source_orders,
            [
                "order_purchase_timestamp",
                "order_approved_at",
                "order_delivered_carrier_date",
                "order_delivered_customer_date",
                "order_estimated_delivery_date",
            ],
        ),
        (source_reviews, ["review_creation_date", "review_answer_timestamp"]),
    ]

    unique_timestamps = set()
    for df, columns in df_time_columns:
        for col in columns:
            unique_timestamps.update(df[df[col].notna()][col].unique())

    unique_time_rows = []
    for timestamp in unique_timestamps:
        datetime = pd.to_datetime(timestamp)
        unique_time_rows.append(
            {
                "time_timestamp": datetime,
                "time_date": datetime.date(),
                "time_year": datetime.year,
                "time_month": datetime.month,
                "time_day": datetime.day,
                "time_hour": datetime.hour,
                "time_minute": datetime.minute,
                "time_second": datetime.second,
            }
        )
    unique_times = pd.DataFrame(unique_time_rows)

    dim_times_catalog = data_catalog()["target_dim_times_table"]
    unique_times.to_sql(
        name=dim_times_catalog["table_name"],
        con=connection,
        **dim_times_catalog.get("save_args", {}),
    )
    logger.info("Dimension Table for Times seeded")


def seed_dim_geolocations_table(source_geolocations: pd.DataFrame, connection: Engine):
    renamed_geolocations = source_geolocations.rename(columns={"id": "geolocation_id"})
    unique_geolocations = renamed_geolocations.groupby(
        ["geolocation_zip_code_prefix"], as_index=False
    ).first()
    dim_geolocations_catalog = data_catalog()["target_dim_geolocations_table"]
    unique_geolocations.to_sql(
        name=dim_geolocations_catalog["table_name"],
        con=connection,
        **dim_geolocations_catalog.get("save_args", {}),
    )
    logger.info("Dimension Table for Geolocations seeded")


def seed_dim_payments_table(source_payments: pd.DataFrame, connection: Engine):
    renamed_payments = source_payments.rename(columns={"id": "payment_id"})
    removed_columns_payments = renamed_payments.drop("order_id", axis=1)
    dim_payments_catalog = data_catalog()["target_dim_payments_table"]
    removed_columns_payments.to_sql(
        name=dim_payments_catalog["table_name"],
        con=connection,
        **dim_payments_catalog.get("save_args", {}),
    )
    logger.info("Dimension Table for Payments seeded")


def seed_dim_products_table(source_products: pd.DataFrame, connection: Engine):
    dim_products_catalog = data_catalog()["target_dim_products_table"]
    source_products.to_sql(
        name=dim_products_catalog["table_name"],
        con=connection,
        **dim_products_catalog.get("save_args", {}),
    )
    logger.info("Dimension Table for Products seeded")


def seed_dim_customers_table(source_customers: pd.DataFrame, connection: Engine):
    dim_customers_catalog = data_catalog()["target_dim_customers_table"]
    source_customers.to_sql(
        name=dim_customers_catalog["table_name"],
        con=connection,
        **dim_customers_catalog.get("save_args", {}),
    )
    logger.info("Dimension Table for Customers seeded")


def seed_dimension_tables(
    source_geolocations: pd.DataFrame,
    source_customers: pd.DataFrame,
    source_orders: pd.DataFrame,
    source_products: pd.DataFrame,
    source_items: pd.DataFrame,
    source_reviews: pd.DataFrame,
    source_payments: pd.DataFrame,
    connection: Engine,
):
    logger.info("Seeding Dimension Tables...")
    seed_dim_times_table(source_orders, source_reviews, connection)
    seed_dim_geolocations_table(source_geolocations, connection)
    seed_dim_payments_table(source_payments, connection)
    seed_dim_products_table(source_products, connection)
    seed_dim_customers_table(source_customers, connection)
    logger.info("All Dimension Tables seeded")


def fact_orders_dfs(
    source_orders: pd.DataFrame,
    source_products: pd.DataFrame,
    source_items: pd.DataFrame,
    source_payments: pd.DataFrame,
    connection: Engine,
) -> Dict[str, pd.DataFrame]:
    """Fact Table for Orders, with and without grouping."""
    column_rename = {
        "order_purchase_timestamp": "purchase_time_timestamp",
        "order_approved_at": "approved_at_time_timestamp",
        "order_delivered_carrier_date": "delivered_carrier_time_timestamp",
        "order_delivered_customer_date": "delivered_customer_time_timestamp",
        "order_estimated_delivery_date": "estimated_delivery_time_timestamp",
    }
    columns_to_keep = ["geolocation_id", "payment_id", "product_id", "customer_id"]
    columns_to_keep.extend(column_rename.values())
    orders = source_orders.rename(columns=column_rename)

    dim_customers = pd.read_sql("dim_customers", connection).rename(
        columns={"id": "customer_id", "customer_id": "customer_id_source"}
    )
    orders = orders.rename(columns={"customer_id": "customer_id_source"}).merge(
        dim_customers, how="left", on="customer_id_source"
    )

    dim_geolocations = pd.read_sql(
        "dim_geolocations", connection, columns=["id", "geolocation_zip_code_prefix"]
    ).rename(columns={"id": "geolocation_id"})
    unique_geolocations = dim_geolocations.drop_duplicates(
        ["geolocation_zip_code_prefix"]
    )
    orders = orders.merge(
        unique_geolocations,
        how="left",
        left_on="customer_zip_code_prefix",
        right_on="geolocation_zip_code_prefix",
    )

    dim_payments = pd.read_sql(
        "dim_payments", connection, columns=["id", "payment_id"]
    ).rename(columns={"id": "payment_id", "payment_id": "payment_id_source"})
    source_and_dim_payments = dim_payments.merge(
        source_payments.rename(columns={"id": "payment_id_source"}),
        how="left",
        on="payment_id_source",
    )
    orders = orders.merge(source_and_dim_payments, how="left", on="order_id")

    items = source_items.rename(columns={"product_id": "product_id_source"})
    dim_products = pd.read_sql(
        "dim_products", connection, columns=["id", "product_id"]
    ).rename(columns={"id": "product_id", "product_id": "product_id_source"})
    items_and_dim_products = items.merge(
        dim_products, how="left", on="product_id_source"
    )
    orders = orders.merge(items_and_dim_products, how="left", on="order_id")

    grouped = orders.groupby(columns_to_keep).size().to_frame("order_qty").reset_index()

    return {"grouped": grouped, "raw": orders}


def seed_fact_orders_table(fact_orders: pd.DataFrame, connection: Engine):
    fact_orders_catalog = data_catalog()["target_fact_orders_table"]
    fact_orders.to_sql(
        name=fact_orders_catalog["table_name"],
        con=connection,
        **fact_orders_catalog.get("save_args", {}),
    )
    logger.info("Fact Table for Orders seeded")


def fact_reviews_dfs(
    source_reviews: pd.DataFrame, raw_fact_orders: pd.DataFrame, connection: Engine,
) -> Dict[str, pd.DataFrame]:
    """Fact Table for Reviews, with and without grouping."""
    column_rename = {
        "review_creation_date": "creation_time_timestamp",
        "review_answer_timestamp": "answer_time_timestamp",
    }
    columns_to_keep = ["geolocation_id", "payment_id", "product_id", "customer_id"]
    columns_to_keep.extend(column_rename.values())
    reviews = source_reviews.rename(columns=column_rename)
    reviews = reviews.merge(raw_fact_orders, how="left", on="order_id")

    grouped = (
        reviews.groupby(columns_to_keep).size().to_frame("review_qty").reset_index()
    )

    return {"grouped": grouped, "raw": reviews}


def seed_fact_reviews_table(fact_reviews: pd.DataFrame, connection: Engine):
    fact_reviews_catalog = data_catalog()["target_fact_reviews_table"]
    fact_reviews.to_sql(
        name=fact_reviews_catalog["table_name"],
        con=connection,
        **fact_reviews_catalog.get("save_args", {}),
    )
    logger.info("Fact Table for Reviews seeded")


def seed_fact_tables(
    source_orders: pd.DataFrame,
    source_products: pd.DataFrame,
    source_items: pd.DataFrame,
    source_reviews: pd.DataFrame,
    source_payments: pd.DataFrame,
    connection: Engine,
):
    logger.info("Seeding Fact Tables...")
    _fact_orders_dfs = fact_orders_dfs(
        source_orders, source_products, source_items, source_payments, connection
    )
    seed_fact_orders_table(_fact_orders_dfs["grouped"], connection)
    _fact_reviews_dfs = fact_reviews_dfs(
        source_reviews, _fact_orders_dfs["raw"], connection
    )
    seed_fact_reviews_table(_fact_reviews_dfs["grouped"], connection)
    logger.info("All Fact Tables seeded")
