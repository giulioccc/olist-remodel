"""Pipelines for seeding target database with remodeled source database data.
"""

from kedro.pipeline import Pipeline, node

from .nodes import remodel_source_database_into_target


def create_pipeline(**kwargs) -> Pipeline:
    return Pipeline(
        [
            node(
                func=remodel_source_database_into_target,
                inputs=[
                    "source_geolocations_table",
                    "source_customers_table",
                    "source_orders_table",
                    "source_products_table",
                    "source_items_table",
                    "source_reviews_table",
                    "source_payments_table",
                ],
                outputs=None,
                name="target_seed_tables",
            ),
        ]
    )
