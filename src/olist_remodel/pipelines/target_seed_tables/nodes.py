"""Nodes for seeding target database with remodeled source database data.
"""

import logging

import pandas as pd

from .utils import (
    connect_to_database,
    create_tables,
    seed_dimension_tables,
    seed_fact_tables,
    target_credentials,
)

logger = logging.getLogger(__name__)


def remodel_source_database_into_target(
    geolocations: pd.DataFrame,
    customers: pd.DataFrame,
    orders: pd.DataFrame,
    products: pd.DataFrame,
    items: pd.DataFrame,
    reviews: pd.DataFrame,
    payments: pd.DataFrame,
):
    """Remodels source database to fact-dimension tables."""
    connection_uri = target_credentials()["con"]
    connection = connect_to_database(connection_uri)
    create_tables(connection)
    seed_dimension_tables(
        geolocations, customers, orders, products, items, reviews, payments, connection
    )
    seed_fact_tables(orders, products, items, reviews, payments, connection)
