import json
import subprocess
from typing import Dict

from kedro.config import ConfigLoader
from pgnotify import await_pg_notifications


def source_credentials() -> Dict[str, str]:
    """Fetches source database credentials."""
    conf_paths = ["conf/local"]
    conf_loader = ConfigLoader(conf_paths)
    credentials = conf_loader.get("credentials*", "credentials*/**")
    return credentials["source_credentials"]


def listen():
    """Listens to new events in reviews or orders tables in source database."""
    connection_uri = source_credentials()["con"]
    for _notification in await_pg_notifications(connection_uri, ['db_notifications']):
        notification = json.loads(_notification.payload)
        table_name = notification["table"]

        params = {
            **notification["data"],
            "source_table_name": table_name,
        }
        params_str = ",".join([f"{k}:{v}" for k, v in params.items()])
        command = f"kedro run --pipeline source_to_target_etl --params \"{params_str}\""
        subprocess.call(command, shell=True)


if __name__ == "__main__":
    listen()
