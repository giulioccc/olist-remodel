"""Nodes for performing ETL from source to target database.
"""

import logging
from typing import Dict

import pandas as pd

from .utils import insert_or_update_facts, related_data, transform_into_facts

logger = logging.getLogger(__name__)


def fetch_related_data(params: Dict[str, str]) -> pd.DataFrame:
    """Get data related to entry."""
    data = related_data(params)
    return data


def transform_related_data_into_facts(
    params: Dict[str, str], related_source_data: pd.DataFrame
) -> pd.DataFrame:
    """Transforms related source data into facts."""
    facts = transform_into_facts(params, related_source_data)
    return facts


def add_facts(params: Dict[str, str], facts: pd.DataFrame):
    """Adds facts into target database."""
    insert_or_update_facts(params, facts)
