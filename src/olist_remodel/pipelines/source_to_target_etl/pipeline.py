"""Pipelines for performing ETL from source to target database.
"""

from kedro.pipeline import Pipeline, node

from .nodes import add_facts, fetch_related_data, transform_related_data_into_facts


def create_pipeline(**kwargs) -> Pipeline:
    return Pipeline(
        [
            node(
                func=fetch_related_data,
                inputs=["parameters"],
                outputs="source_related_data",
                name="fetch_related_data",
            ),
            node(
                func=transform_related_data_into_facts,
                inputs=["parameters", "source_related_data"],
                outputs="facts",
                name="transform_related_data_into_facts",
            ),
            node(
                func=add_facts,
                inputs=["parameters", "facts"],
                outputs=None,
                name="add_facts",
            ),
        ]
    )
