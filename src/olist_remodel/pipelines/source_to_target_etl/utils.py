import logging
from typing import Dict

import pandas as pd
from kedro.config import ConfigLoader
from sqlalchemy import create_engine
from sqlalchemy.engine.base import Engine
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm import sessionmaker

from olist_remodel.database.models import (
    DimCustomer,
    DimGeolocation,
    DimPayment,
    DimProduct,
    DimTime,
    FactOrder,
    FactReview,
)

logger = logging.getLogger(__name__)


def source_credentials() -> Dict[str, str]:
    """Fetches source database credentials."""
    conf_paths = ["conf/local"]
    conf_loader = ConfigLoader(conf_paths)
    credentials = conf_loader.get("credentials*", "credentials*/**")
    return credentials["source_credentials"]


def target_credentials() -> Dict[str, str]:
    """Fetches target database credentials."""
    conf_paths = ["conf/local"]
    conf_loader = ConfigLoader(conf_paths)
    credentials = conf_loader.get("credentials*", "credentials*/**")
    return credentials["target_credentials"]


def connect_to_database(uri: str) -> Engine:
    """Connects to SQL Database."""
    engine = create_engine(uri)
    logger.info(f"Connection established with {uri}")
    return engine


def data_catalog() -> Dict[str, dict]:
    """Fetches this pipeline's Data Catalog."""
    conf_paths = ["conf/base", "conf/local"]
    conf_loader = ConfigLoader(conf_paths)
    catalog = conf_loader.get(
        "catalog*",
        "catalog*/**",
        "pipelines/target_seed_tables/catalog*",
        "pipelines/target_seed_tables/catalog*/**",
    )
    return catalog


def review_related_data(params: Dict[str, str], connection: Engine) -> pd.DataFrame:
    """Fetches all related data to a review."""
    source_order_id = params["order_id"]

    entries = pd.read_sql_query(
        f"""
        SELECT
            pay.id AS payment_id,
            p.product_id,
            c.customer_id,
            c.customer_zip_code_prefix AS geolocation_zip_code_prefix
        FROM orders o
            LEFT JOIN customers c ON (o.customer_id = c.customer_id)
            LEFT JOIN ( SELECT
                            order_id,
                            product_id
                        FROM items
                        GROUP BY
                            order_id,
                            product_id ) i ON (i.order_id = o.order_id)
            LEFT JOIN products p ON (i.product_id = p.product_id)
            LEFT JOIN payments pay ON (o.order_id = pay.order_id)
        WHERE o.order_id='{source_order_id}'
        """,
        con=connection,
    )
    entries["review_creation_date"] = params.get("review_creation_date", None)
    entries["review_answer_timestamp"] = params.get("review_answer_timestamp", None)

    return entries


def insert_into_dim_geolocations(
    geolocation_zip_code_prefix: str,
    source_connection: Engine,
    target_connection: Engine,
):
    """Inserts geolocation entry into dim_geolocations table if it doesn't exist."""
    session = sessionmaker(bind=target_connection)()
    geolocation_key = {"geolocation_zip_code_prefix": geolocation_zip_code_prefix}
    record = session.query(DimGeolocation).filter_by(**geolocation_key).first()

    if record:
        session.close()
        return

    entry = (
        pd.read_sql_query(
            f"""
        SELECT
            id AS geolocation_id,
            geolocation_zip_code_prefix,
            geolocation_lat,
            geolocation_lng,
            geolocation_city,
            geolocation_state
        FROM geolocations
        WHERE geolocation_zip_code_prefix='{geolocation_zip_code_prefix}'
        LIMIT 1
        """,
            con=source_connection,
        )
        .iloc[0]
        .to_dict()
    )
    entry["geolocation_id"] = int(entry["geolocation_id"])
    dim_geolocation = DimGeolocation(**entry)
    session.add(dim_geolocation)
    session.commit()

    session.close()


def insert_into_dim_customers(
    customer_id: str, source_connection: Engine, target_connection: Engine
):
    """Inserts customer entry into dim_customers table if it doesn't exist."""
    session = sessionmaker(bind=target_connection)()
    customer_key = {"customer_id": customer_id}
    record = session.query(DimCustomer).filter_by(**customer_key).first()

    if record:
        session.close()
        return

    entry = (
        pd.read_sql_query(
            f"""
        SELECT *
        FROM customers
        WHERE customer_id='{customer_id}'
        LIMIT 1
        """,
            con=source_connection,
        )
        .iloc[0]
        .to_dict()
    )
    dim_customer = DimCustomer(**entry)
    session.add(dim_customer)
    session.commit()

    session.close()


def insert_into_dim_products(
    product_id: str, source_connection: Engine, target_connection: Engine
):
    """Inserts product entry into dim_products table if it doesn't exist."""
    session = sessionmaker(bind=target_connection)()
    product_key = {"product_id": product_id}
    record = session.query(DimProduct).filter_by(**product_key).first()

    if record:
        session.close()
        return

    entry = (
        pd.read_sql_query(
            f"""
        SELECT *
        FROM products
        WHERE product_id='{product_id}'
        LIMIT 1
        """,
            con=source_connection,
        )
        .iloc[0]
        .to_dict()
    )
    dim_product = DimProduct(**entry)
    session.add(dim_product)
    session.commit()

    session.close()


def insert_into_dim_payments(
    payment_id: str, source_connection: Engine, target_connection: Engine
):
    """Inserts payment entry into dim_payments table if it doesn't exist."""
    session = sessionmaker(bind=target_connection)()
    payment_key = {"payment_id": payment_id}
    record = session.query(DimPayment).filter_by(**payment_key).first()

    if record:
        session.close()
        return

    entry = (
        pd.read_sql_query(
            f"""
        SELECT
            id AS payment_id,
            payment_sequential,
            payment_type,
            payment_installments,
            payment_value
        FROM payments
        WHERE id='{payment_id}'
        LIMIT 1
        """,
            con=source_connection,
        )
        .iloc[0]
        .to_dict()
    )
    entry.update(
        {
            "payment_id": int(entry["payment_id"]),
            "payment_sequential": int(entry["payment_sequential"]),
            "payment_installments": int(entry["payment_installments"]),
            "payment_value": int(entry["payment_value"]),
        }
    )
    dim_payment = DimPayment(**entry)
    session.add(dim_payment)
    session.commit()

    session.close()


def order_related_data(
    params: Dict[str, str], source_connection: Engine, target_connection: Engine
) -> Dict[str, pd.DataFrame]:
    """Fetches all related data to a order."""
    source_order_id = params["order_id"]
    source_customer_id = params["customer_id"]

    entries = pd.read_sql_query(
        f"""
        SELECT
            pay.id AS payment_id,
            p.product_id,
            c.customer_id,
            c.customer_zip_code_prefix AS geolocation_zip_code_prefix
        FROM orders o
            LEFT JOIN customers c ON (o.customer_id = c.customer_id)
            LEFT JOIN ( SELECT
                            order_id,
                            product_id
                        FROM items
                        GROUP BY
                            order_id,
                            product_id ) i ON (i.order_id = o.order_id)
            LEFT JOIN products p ON (i.product_id = p.product_id)
            LEFT JOIN payments pay ON (o.order_id = pay.order_id)
        WHERE
            o.order_id='{source_order_id}'
            AND o.customer_id='{source_customer_id}'
        """,
        con=source_connection,
    )
    for index, entry in entries.iterrows():
        insert_into_dim_geolocations(
            entry["geolocation_zip_code_prefix"], source_connection, target_connection
        )
        insert_into_dim_customers(
            entry["customer_id"], source_connection, target_connection
        )
        insert_into_dim_products(
            entry["product_id"], source_connection, target_connection
        )
        insert_into_dim_payments(
            entry["payment_id"], source_connection, target_connection
        )

    entries["order_purchase_timestamp"] = params.get("order_purchase_timestamp", None)
    entries["order_approved_at"] = params.get("order_approved_at", None)
    entries["order_delivered_carrier_date"] = params.get(
        "order_delivered_carrier_date", None
    )
    entries["order_delivered_customer_date"] = params.get(
        "order_delivered_customer_date", None
    )
    entries["order_estimated_delivery_date"] = params.get(
        "order_estimated_delivery_date", None
    )

    return entries


def related_data(params: Dict[str, str]) -> pd.DataFrame:
    """Chooses correct method for fetching related data."""
    table_name = params["source_table_name"]
    source_connection_uri = source_credentials()["con"]
    source_connection = connect_to_database(source_connection_uri)
    if table_name == "reviews":
        data = review_related_data(params, source_connection)
    elif table_name == "orders":
        target_connection_uri = target_credentials()["con"]
        target_connection = connect_to_database(target_connection_uri)
        data = order_related_data(params, source_connection, target_connection)
    return data


def review_transform_into_facts(
    source_entries: pd.DataFrame, connection: Engine
) -> pd.DataFrame:
    """Maps source review related data to dimensions pkeys and groups reviews."""
    target_entries = pd.DataFrame()
    time_columns_mapping = {
        "review_creation_date": "creation_time_timestamp",
        "review_answer_timestamp": "answer_time_timestamp",
    }
    for index, entry in source_entries.iterrows():
        dimension_keys = pd.read_sql_query(
            """
            SELECT
                dg.id AS geolocation_id,
                dc.id AS customer_id,
                dp.id AS product_id,
                dpay.id AS payment_id
            FROM
                dim_geolocations dg,
                dim_customers dc,
                dim_products dp,
                dim_payments dpay
            WHERE
                dg.geolocation_zip_code_prefix='{geolocation_zip_code_prefix}'
                AND dc.customer_id='{customer_id}'
                AND dp.product_id='{product_id}'
                AND dpay.payment_id='{payment_id}'
            """.format(
                **entry
            ),
            con=connection,
        )
        for source_column, target_column in time_columns_mapping.items():
            dimension_keys[target_column] = entry[source_column]

        target_entries = pd.concat([target_entries, dimension_keys])

    grouped = (
        target_entries.groupby(target_entries.columns.to_list())
        .size()
        .to_frame("review_qty")
        .reset_index()
    )
    return grouped


def order_transform_into_facts(
    source_entries: pd.DataFrame, target_connection: Engine
) -> pd.DataFrame:
    """Maps source order related data to dimensions pkeys and groups reviews."""
    target_entries = pd.DataFrame()
    time_columns_mapping = {
        "order_purchase_timestamp": "purchase_time_timestamp",
        "order_approved_at": "approved_at_time_timestamp",
        "order_delivered_carrier_date": "delivered_carrier_time_timestamp",
        "order_delivered_customer_date": "delivered_customer_time_timestamp",
        "order_estimated_delivery_date": "estimated_delivery_time_timestamp",
    }
    for index, entry in source_entries.iterrows():
        dimension_keys = pd.read_sql_query(
            """
            SELECT
                dg.id AS geolocation_id,
                dc.id AS customer_id,
                dp.id AS product_id,
                dpay.id AS payment_id
            FROM
                dim_geolocations dg,
                dim_customers dc,
                dim_products dp,
                dim_payments dpay
            WHERE
                dg.geolocation_zip_code_prefix='{geolocation_zip_code_prefix}'
                AND dc.customer_id='{customer_id}'
                AND dp.product_id='{product_id}'
                AND dpay.payment_id='{payment_id}'
            """.format(
                **entry
            ),
            con=target_connection,
        )
        for source_column, target_column in time_columns_mapping.items():
            dimension_keys[target_column] = entry[source_column]

        target_entries = pd.concat([target_entries, dimension_keys])

    grouped = (
        target_entries.groupby(target_entries.columns.to_list())
        .size()
        .to_frame("order_qty")
        .reset_index()
    )
    return grouped


def transform_into_facts(
    params: Dict[str, str], source_related_data: pd.DataFrame
) -> pd.DataFrame:
    """Chooses correct method for transforming source related data into facts."""
    table_name = params["source_table_name"]
    target_connection_uri = target_credentials()["con"]
    target_connection = connect_to_database(target_connection_uri)
    if table_name == "reviews":
        facts = review_transform_into_facts(source_related_data, target_connection)
    elif table_name == "orders":
        facts = order_transform_into_facts(source_related_data, target_connection)
    return facts


def insert_into_dim_times(timestamp: str, connection: Engine):
    """Inserts time entry into dim_times table if it doesn't exist."""
    datetime = pd.to_datetime(timestamp)
    time = {
        "time_timestamp": datetime,
        "time_date": datetime.date(),
        "time_year": datetime.year,
        "time_month": datetime.month,
        "time_day": datetime.day,
        "time_hour": datetime.hour,
        "time_minute": datetime.minute,
        "time_second": datetime.second,
    }
    dim_time = DimTime(**time)
    session = sessionmaker(bind=connection)()
    try:
        session.add(dim_time)
        session.commit()
    except IntegrityError:
        logger.warning("Timestamp %s already exists", time["time_timestamp"])
        session.rollback()
    except Exception:
        session.rollback()
        raise
    finally:
        session.close()


def review_insert_or_update_facts(facts: pd.DataFrame, connection: Engine):
    """Inserts review facts into target database or updates if they already exist."""
    for index, fact in facts.iterrows():
        insert_into_dim_times(fact["creation_time_timestamp"], connection)
        insert_into_dim_times(fact["answer_time_timestamp"], connection)

        session = sessionmaker(bind=connection)()
        fact_key = fact.copy()
        del fact_key["review_qty"]
        record = session.query(FactReview).filter_by(**fact_key).first()
        if record:
            record.review_qty += fact["review_qty"]
        else:
            fact_review = FactReview(**fact)
            session.add(fact_review)

        session.commit()
        session.close()


def order_insert_or_update_facts(facts: pd.DataFrame, connection: Engine):
    """Inserts order facts into target database or updates if they already exist."""
    for index, fact in facts.iterrows():
        insert_into_dim_times(fact["purchase_time_timestamp"], connection)
        insert_into_dim_times(fact["approved_at_time_timestamp"], connection)
        insert_into_dim_times(fact["delivered_carrier_time_timestamp"], connection)
        insert_into_dim_times(fact["delivered_customer_time_timestamp"], connection)
        insert_into_dim_times(fact["estimated_delivery_time_timestamp"], connection)

        session = sessionmaker(bind=connection)()
        fact_key = fact.copy()
        del fact_key["order_qty"]
        record = session.query(FactOrder).filter_by(**fact_key).first()
        if record:
            record.order_qty += fact["order_qty"]
        else:
            fact_order = FactOrder(**fact)
            session.add(fact_order)

        session.commit()
        session.close()


def insert_or_update_facts(params: Dict[str, str], facts: pd.DataFrame):
    """Inserts facts into target database or updates if they already exist."""
    table_name = params["source_table_name"]
    target_connection_uri = target_credentials()["con"]
    target_connection = connect_to_database(target_connection_uri)
    if table_name == "reviews":
        review_insert_or_update_facts(facts, target_connection)
    elif table_name == "orders":
        order_insert_or_update_facts(facts, target_connection)
