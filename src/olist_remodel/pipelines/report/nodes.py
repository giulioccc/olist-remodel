"""Nodes for reporting metrics using fact-dimension database.
"""

import logging
import time

import hvplot.pandas  # noqa
import pandas as pd

from .utils import connect_to_database, source_credentials, target_credentials

logger = logging.getLogger(__name__)


def report_orders_by_day():
    """Report orders by day."""
    connection_uri = target_credentials()["con"]
    connection = connect_to_database(connection_uri)
    orders_by_day_df = pd.read_sql_query(
        """
        SELECT
            SUM(fo.order_qty) AS order_qty,
            dt.time_date
        FROM fact_orders fo
        LEFT JOIN dim_times dt ON (fo.purchase_time_timestamp = dt.time_timestamp)
        GROUP BY dt.time_date
        """,
        con=connection,
    )
    orders_by_day_df = orders_by_day_df.rename(
        columns={"order_qty": "Number of Orders", "time_date": "Date"}
    )
    plot = orders_by_day_df.hvplot(x="Date", y="Number of Orders")
    return plot


def report_reviews_by_product():
    """Report reviews by product."""
    connection_uri = target_credentials()["con"]
    connection = connect_to_database(connection_uri)
    reviews_by_product_df = pd.read_sql_query(
        """
        SELECT
            SUM(fr.review_qty) AS review_qty,
            dp.product_id
        FROM fact_reviews fr
        LEFT JOIN dim_products dp ON (fr.product_id = dp.id)
        GROUP BY dp.product_id
        """,
        con=connection,
    )
    reviews_by_product_df = reviews_by_product_df.rename(
        columns={"review_qty": "Number of Reviews", "product_id": "Product ID"}
    )
    plot = reviews_by_product_df.hvplot.table(
        columns=["Product ID", "Number of Reviews"], sortable=True, selectable=True
    )
    return plot


def report_orders_by_geolocation():
    """Report orders by geolocation."""
    connection_uri = target_credentials()["con"]
    connection = connect_to_database(connection_uri)
    orders_by_geolocation_df = pd.read_sql_query(
        """
        SELECT
            SUM(fo.order_qty) AS order_qty,
            dg.geolocation_zip_code_prefix
        FROM fact_orders fo
        LEFT JOIN dim_geolocations dg ON (fo.geolocation_id = dg.id)
        GROUP BY dg.geolocation_zip_code_prefix
        """,
        con=connection,
    )
    orders_by_geolocation_df = orders_by_geolocation_df.rename(
        columns={
            "order_qty": "Number of Orders",
            "geolocation_zip_code_prefix": "Zip Code Prefix",
        }
    )
    plot = orders_by_geolocation_df.hvplot.table(
        columns=["Zip Code Prefix", "Number of Orders"], sortable=True, selectable=True
    )
    return plot


def report_orders_by_payment():
    """Report orders by payment."""
    connection_uri = target_credentials()["con"]
    connection = connect_to_database(connection_uri)
    orders_by_payment_df = pd.read_sql_query(
        """
        SELECT
            SUM(fo.order_qty) AS order_qty,
            dpay.payment_type
        FROM fact_orders fo
        LEFT JOIN dim_payments dpay ON (fo.payment_id = dpay.id)
        GROUP BY dpay.payment_type
        """,
        con=connection,
    )
    orders_by_payment_df = orders_by_payment_df.rename(
        columns={"order_qty": "Number of Orders", "payment_type": "Payment Type"}
    )
    plot = orders_by_payment_df.hvplot.bar(
        y="Number of Orders", x="Payment Type", invert=True
    )
    return plot


def report_reviews_by_customer():
    """Report reviews by customer."""
    connection_uri = target_credentials()["con"]
    connection = connect_to_database(connection_uri)
    reviews_by_customer_df = pd.read_sql_query(
        """
        SELECT
            SUM(fr.review_qty) AS review_qty,
            dc.customer_id
        FROM fact_reviews fr
        LEFT JOIN dim_customers dc ON (fr.customer_id = dc.id)
        GROUP BY dc.customer_id
        """,
        con=connection,
    )
    reviews_by_customer_df = reviews_by_customer_df.rename(
        columns={"review_qty": "Number of Reviews", "customer_id": "Customer ID"}
    )
    plot = reviews_by_customer_df.hvplot.table(
        columns=["Customer ID", "Number of Reviews"], sortable=True, selectable=True
    )
    return plot


def report_query_time_comparison():
    """Report query time comparison between source and target database."""
    source_connection_uri = source_credentials()["con"]
    source_connection = connect_to_database(source_connection_uri)

    start_source = time.time()
    for i in range(100):
        pd.read_sql_query(
            """
            SELECT COUNT(*)
            FROM orders o
                LEFT JOIN ( SELECT
                                order_id,
                                product_id
                            FROM items
                            GROUP BY
                                order_id,
                                product_id ) i ON (i.order_id = o.order_id)
                LEFT JOIN products p ON (i.product_id = p.product_id)
                LEFT JOIN payments pay ON (o.order_id = pay.order_id)
            WHERE
                pay.payment_type='credit_card'
                AND p.product_category_name='informatica_acessorios'
            """,
            con=source_connection,
        )
    elapsed_time_source = time.time() - start_source

    target_connection_uri = target_credentials()["con"]
    target_connection = connect_to_database(target_connection_uri)

    start_target = time.time()
    for i in range(100):
        pd.read_sql_query(
            """
            SELECT SUM(fo.order_qty)
            FROM fact_orders fo
            LEFT JOIN dim_products dp ON (fo.product_id = dp.id)
            LEFT JOIN dim_payments dpay ON (fo.payment_id = dpay.id)
            WHERE
                dpay.payment_type='credit_card'
                AND dp.product_category_name='informatica_acessorios'
            """,
            con=target_connection,
        )
    elapsed_time_target = time.time() - start_target

    logger.info(f"Source time elapsed: {elapsed_time_source}")
    logger.info(f"Target time elapsed: {elapsed_time_target}")
    logger.info(
        "Performance Factor: "
        f"{((elapsed_time_target-elapsed_time_source)/elapsed_time_source)*100}%"
    )
    return ""
