import logging
from typing import Dict

from kedro.config import ConfigLoader
from sqlalchemy import create_engine
from sqlalchemy.engine.base import Engine

logger = logging.getLogger(__name__)


def target_credentials() -> Dict[str, str]:
    """Fetches target database credentials."""
    conf_paths = ["conf/local"]
    conf_loader = ConfigLoader(conf_paths)
    credentials = conf_loader.get("credentials*", "credentials*/**")
    return credentials["target_credentials"]


def source_credentials() -> Dict[str, str]:
    """Fetches source database credentials."""
    conf_paths = ["conf/local"]
    conf_loader = ConfigLoader(conf_paths)
    credentials = conf_loader.get("credentials*", "credentials*/**")
    return credentials["source_credentials"]


def connect_to_database(uri: str) -> Engine:
    """Connects to SQL Database."""
    engine = create_engine(uri)
    logger.info(f"Connection established with {uri}")
    return engine
