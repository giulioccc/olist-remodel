"""Pipelines for reporting metrics using fact-dimension database.
"""

from kedro.pipeline import Pipeline, node

from .nodes import (
    report_orders_by_day,
    report_orders_by_geolocation,
    report_orders_by_payment,
    report_query_time_comparison,
    report_reviews_by_customer,
    report_reviews_by_product,
)


def create_pipeline(**kwargs) -> Pipeline:
    return Pipeline(
        [
            node(
                func=report_orders_by_day,
                inputs=None,
                outputs="orders_by_day_report",
                name="report_orders_by_day",
            ),
            node(
                func=report_reviews_by_product,
                inputs=None,
                outputs="reviews_by_product_report",
                name="report_reviews_by_product",
            ),
            node(
                func=report_orders_by_geolocation,
                inputs=None,
                outputs="orders_by_geolocation_report",
                name="report_orders_by_geolocation",
            ),
            node(
                func=report_orders_by_payment,
                inputs=None,
                outputs="orders_by_payment_report",
                name="report_orders_by_payment",
            ),
            node(
                func=report_reviews_by_customer,
                inputs=None,
                outputs="reviews_by_customer_report",
                name="report_reviews_by_customer",
            ),
            node(
                func=report_query_time_comparison,
                inputs=None,
                outputs="query_time_comparison_report",
                name="report_query_time_comparison",
            ),
        ]
    )
