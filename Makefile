setup:
	pip install -r src/requirements.txt
	docker-compose up -d dbsource dbtarget

seed_source:
	kedro run --pipeline source_seed_geolocations --params "source_table_name:geolocations"
	kedro run --pipeline source_seed_customers --params "source_table_name:customers"
	kedro run --pipeline source_seed_sellers --params "source_table_name:sellers"
	kedro run --pipeline source_seed_orders --params "source_table_name:orders"
	kedro run --pipeline source_seed_products --params "source_table_name:products"
	kedro run --pipeline source_seed_items --params "source_table_name:items"
	kedro run --pipeline source_seed_reviews --params "source_table_name:reviews"
	kedro run --pipeline source_seed_payments --params "source_table_name:payments"

seed_target:
	kedro run --pipeline target_seed_tables

report:
	kedro run --pipeline report

etl:
	docker cp src/olist_remodel/database/notify_trigger.sql olist-postgres-source:/
	docker cp src/olist_remodel/database/create_triggers.sql olist-postgres-source:/
	docker exec -it olist-postgres-source psql -U olist -a -q -f /notify_trigger.sql
	docker exec -it olist-postgres-source psql -U olist -a -q -f /create_triggers.sql
	python src/olist_remodel/pipelines/source_to_target_etl/listen_notifications.py

simulate_insertions:
	docker cp src/olist_remodel/database/simulate_insertions.sql olist-postgres-source:/
	docker exec -it olist-postgres-source psql -U olist -a -q -f /simulate_insertions.sql
