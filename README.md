# Olist Remodeling Architecture

## Instalação

Este projeto requer python>=3.6, docker e docker-compose (um virtual environment também é bom).

O projeto exige os arquivos `conf/local/.env.dbsource`, `conf/local/.env.dbtarget` e `conf/local/credentials.yml`. Verifique se está com a sua cópia destes arquivos no lugar correto antes de instalar o projeto.

E também são necessários os arquivos CSV originais, que ficam localizados em `data/01_raw/source_seed_csvs`.

```
$ make setup
```

## Executando o projeto

Para preencher o banco de origem com os dados dos CSVs, execute

```
$ make seed_source
```

Para preencher o banco remodelado, execute

```
$ make seed_target
```

Para ver todos os reports gerados, execute

```
$ make report
```

Os reports são encontrados de acordo com a tabela a seguir:

| Report                                  | Localização                                  |
| ----------------------------------------|----------------------------------------------|
| Número de Pedidos Por Dia               | data/08_reporting/orders_by_day.html         |
| Número de Review Por Produto            | data/08_reporting/reviews_by_product.html    |
| Número de Pedidos Por Localidade        | data/08_reporting/orders_by_geolocation.html |
| Número de Pedidos Por Meio de Pagamento | data/08_reporting/orders_by_payment.html     |
| Número de Reviews Por Cliente           | data/08_reporting/reviews_by_client.html     |
| Performance de query Origem x Remodel   | Logs de `make report`                        |

Para configurar o ETL, execute

```
$ make etl
```

Este comando irá deixar um _listener_ executando no terminal a espera de qualquer inserção nas tabelas de Review ou Pedido.

Para simular as inserções, execute

```
$ make simulate_insertions
```
